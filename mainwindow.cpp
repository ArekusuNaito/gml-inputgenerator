#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qstring.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_generateButton_clicked()
{
    //Will generate keyboard code?
    QString code;
    //Variable name
    QString variableName = ui->variableNameTextbox->text();
    //HoldCode
    QString holdCode="";
    //Pressed Code
    QString pressedCode = "";
    //Released Code
    QString releasedCode = "";
    //Keyboard Codes
    QString keyboardHoldKeyCode = "";
    QString keyboardPressedKeyCode = "";
    QString keyboardReleasedKeyCode = "";
    //Gamepad Codes
    QString gamepadHoldButtonCode = "";
    QString gamepadPressedButtonCode = "";
    QString gamepadReleasedButtonCode = "";
    if(ui->variableNameTextbox->text().length() != 0)
    {
        if(ui->keyboardCheckbox->checkState())
        {
            keyboardHoldKeyCode = "keyboard_check(ord('Z'))";
            keyboardPressedKeyCode = "keyboard_check_pressed(ord('Z'))";
            keyboardReleasedKeyCode = "keyboard_check_released(ord('Z'))";
        }
        if(ui->gamepadCheckbox->checkState())
        {
            gamepadHoldButtonCode = "gamepad_button_check(0,gp_face1)";
            gamepadPressedButtonCode = "gamepad_button_check_pressed(0,gp_face1)";
            gamepadReleasedButtonCode = "gamepad_button_check_released(0,gp_face1)";
        }

        //Generate holdCode
        holdCode = variableName+"Hold = "+ gamepadHoldButtonCode + " or " + keyboardHoldKeyCode;
        pressedCode =variableName+"Pressed = "+ gamepadPressedButtonCode + " or " + keyboardPressedKeyCode;
        releasedCode =variableName+"Released = "+ gamepadReleasedButtonCode + " or " + keyboardReleasedKeyCode;
        code = holdCode+"\n"+pressedCode+"\n"+releasedCode;

        ui->plainTextEdit->setPlainText(code);

    }
    //else Messagebox("VariableName empty")

}
